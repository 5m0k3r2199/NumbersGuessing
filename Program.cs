﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumbersGuessing
{
    internal class Program
    {
        static void Main(string[] args)
        {

        start:
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Welcome to my little numbers guessing game! Please enter a username.");
            string user = Console.ReadLine();
            int test = 0;
            int counter = 10;

            Random r = new Random();

            int winnum = r.Next(0, 100);
            bool win = false;

            do
            {
                test++;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Guess a Number between 0 and 100! You Have " +counter+ " turns left!");
                int ig = Convert.ToInt32(Console.ReadLine());
                

                if (ig < winnum)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Attention " +user+ " your number is to low! Guess higher...");
                    Console.WriteLine("------------------------------------------------------------");
                    counter--;
                }
                else if (ig > winnum)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Attention " +user+ " your number is to high! Guess lower...");
                    Console.WriteLine("------------------------------------------------------------");
                    counter--;
                }
                else if (ig == winnum)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("Correct guess "+user+" ! You Win!");
                    Console.WriteLine("------------------------------------------------------------");
                    win = true;
                }
                if(test == 10)
                {
                    break;
                }

            } while (win == false);

            if (win == true)
            {


                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("You needed " + test + " turns to win!");
                
                
                Console.WriteLine("Did you have fun? " + user + " Wanna play again? y/n");
                Console.WriteLine("------------------------------------------------------------");
                string goagain = Console.ReadLine().ToUpper();
                

                if (goagain == "Y")
                {
                    goto start;
                }
                else
                {
                    Console.WriteLine("------------------------------------------------------------");
                    Console.WriteLine("Press any Key to Exit...");
                    Console.ReadKey();
                }
            }
            else if (test == 10)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You lost! Wanna try again?");
                Console.WriteLine("Did you have fun? " + user + " Wanna play again? y/n");
                Console.WriteLine("------------------------------------------------------------");
                string goagain = Console.ReadLine().ToUpper();
                

                if (goagain == "Y")
                {
                    goto start;
                }
                else
                {
                    Console.WriteLine("------------------------------------------------------------");
                    Console.WriteLine("Press any Key to Exit...");
                    Console.ReadKey();
                }

            }
        }
    }
}
